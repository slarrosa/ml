﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lluvia : MonoBehaviour
{
    public GameObject diamond;
    private bool wait = false;
    private bool wait2 = false;
    public GameObject suelo;
    public SO_Stats speed;
    void Start()
    {
        speed.spawnSpeed = 2;
    }

    void Update()
    {
        StartCoroutine(DiamondSpawning());
        StartCoroutine(SpawnSpeedModifier());
    }

    IEnumerator DiamondSpawning()
    {
        if (!wait)
        {
            wait = true;
            GameObject newDiamond = Instantiate(diamond);
            newDiamond.transform.position = new Vector2(Random.Range(-8.5f, 8.5f), suelo.transform.position.y + 10);
            yield return new WaitForSeconds(speed.spawnSpeed);
            speed.diamonds.Add(newDiamond);
            wait = false;
        }
        yield return null;
    }
    IEnumerator SpawnSpeedModifier()
    {
        if (!wait2) {
            wait2 = true;
            speed.spawnSpeed = speed.spawnSpeed - 0.2f;
            yield return new WaitForSeconds(5);
            wait2 = false;
        }
    }
}
