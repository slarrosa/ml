﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_Stats : ScriptableObject
{
    public int vida;
    public int score;
    public float spawnSpeed;
    public List<GameObject> diamonds;
}
