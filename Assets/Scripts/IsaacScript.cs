﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsaacScript : MonoBehaviour
{
    public int vel;
    public SO_Stats isaacVida;
    void Start()
    {
        isaacVida.vida = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
        }
        else if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
}
