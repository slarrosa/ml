﻿using System;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class Moute : Agent
{
    private Vector2 startPos;
    public SO_Stats IsaacVida;
    private int maxScore = 0;
    public override void Initialize()
    {
        base.Initialize();
        startPos = this.transform.position;
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        int accioVector = (int)Mathf.Floor(vectorAction[0]);
        if(accioVector == 1)
        {
            moveLeft();
        }
        if (accioVector == 2)
        {
            moveRight();
        }
        if(accioVector == 0)
        {
            dontMove();
        }
    }
    public override void OnEpisodeBegin()
    {
        IsaacVida.vida = 3;
        IsaacVida.score = 0;
        this.transform.parent.GetChild(5).GetChild(0).gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "" + IsaacVida.score;
        this.transform.position = startPos;
        IsaacVida.spawnSpeed = 2;
        foreach (GameObject go in IsaacVida.diamonds)
        {
            Destroy(go);
        }
        
    }

    public override void Heuristic(float[] actionsOut)
    {
        if (Input.GetKey("a"))
        {
            actionsOut[0] = 1;
        }else if (Input.GetKey("d"))
        {
            actionsOut[0] = 2;
        }
        else
        {
            actionsOut[0] = 0;
        }
    }

    private void moveLeft()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);
    }
    private void moveRight()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);
    }
    private void dontMove()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "bala")
        {
            AddReward(-2f);
            Debug.Log(GetCumulativeReward());
            IsaacVida.vida--;
            if (IsaacVida.vida <= 0)
            {
                EndEpisode();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.transform.tag == "Nope")
        {
            AddReward(0.1f);
            Debug.Log(GetCumulativeReward());
        }
    }
    private void FixedUpdate()
    {
        this.transform.parent.GetChild(5).GetChild(0).gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "" + IsaacVida.score;
        if (IsaacVida.score>maxScore)
        {
            maxScore = IsaacVida.score;
            this.transform.parent.GetChild(5).GetChild(1).gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "" + maxScore;
        }

    }
}
