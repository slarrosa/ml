﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaScript : MonoBehaviour
{
    public SO_Stats IsaacVida;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            Destroy(this.gameObject);
            IsaacVida.vida--;
        }
    }
}
