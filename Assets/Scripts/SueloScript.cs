﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloScript : MonoBehaviour
{
    public SO_Stats IsaacVida;
    void Start()
    {
        IsaacVida.score = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "bala")
        {
            Destroy(collision.gameObject);
            IsaacVida.score++;
        }
    }
}
